package com.mrh.flutter_dingtalk.constant

object Methods {
    const val REGISTER_APP = "registerApp";
    const val UNREGISTER_APP = "unregisterApp";
    const val IS_Ding_Ding_INSTALLED = "isDingDingInstalled"
    const val SEND_AUTH = "sendAuth"
    const val SEND_TEXT_MESSAGE = "sendTextMessage"
    const val SEND_WEB_PAGE_MESSAGE = "sendWebPageMessage"
    const val SEND_IMAGE_MESSAGE = "sendImageMessage"
}