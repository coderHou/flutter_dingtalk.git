package com.mrh.flutter_dingtalk

import androidx.annotation.NonNull
import com.mrh.flutter_dingtalk.constant.Methods
import com.mrh.flutter_dingtalk.handlers.IDDShareApiHandler
import com.mrh.flutter_dingtalk.handlers.IDDShareResponseHandler

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

/** FlutterDingtalkPlugin */
class FlutterDingtalkPlugin: FlutterPlugin, MethodCallHandler , ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_dingtalk")
    channel.setMethodCallHandler(this)
//    IDDShareApiHandler.setRegister(flutterPluginBinding.applicationContext)
    IDDShareResponseHandler.setMethodChannel(channel)
  }
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "flutter_dingtalk")
      channel.setMethodCallHandler(FlutterDingtalkPlugin())
      IDDShareApiHandler.setRegister(registrar.activity())
      IDDShareResponseHandler.setMethodChannel(channel)
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
//    if (call.method == "getPlatformVersion") {
//      result.success("Android ${android.os.Build.VERSION.RELEASE}")
//    }else if (call.method == Methods.IS_Ding_Ding_INSTALLED){
//
//    } else {
//      result.notImplemented()
//    }

    when (call.method){
      "getPlatformVersion" -> {
        result.success("Android ${android.os.Build.VERSION.RELEASE}")
      }
      Methods.REGISTER_APP -> {
        IDDShareApiHandler.registerApp(call,result)
      }
      Methods.UNREGISTER_APP -> {
        IDDShareApiHandler.unregisterApp(result)
      }
      Methods.IS_Ding_Ding_INSTALLED -> {
        IDDShareApiHandler.checkInstall(result)
      }
      Methods.SEND_AUTH -> {
        IDDShareApiHandler.sendAuth(call,result)
      }
      Methods.SEND_TEXT_MESSAGE -> {
        IDDShareApiHandler.sendTextMessage(call.arguments as Map<String?, Any?>,result)
      }
      Methods.SEND_IMAGE_MESSAGE -> {
        IDDShareApiHandler.sendImageMessage(call.arguments as Map<String?, Any?>,result)
      }
      Methods.SEND_WEB_PAGE_MESSAGE -> {
        IDDShareApiHandler.sendWebPageMessage(call.arguments as Map<String?, Any?>,result)
      }

    }

  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onDetachedFromActivity() {
    IDDShareApiHandler.setRegister(null)
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    IDDShareApiHandler.setRegister(binding.activity)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    IDDShareApiHandler.setRegister(binding.activity)
  }

  override fun onDetachedFromActivityForConfigChanges() {
    IDDShareApiHandler.setRegister(null)
  }
}
