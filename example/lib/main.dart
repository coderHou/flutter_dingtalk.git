
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_dingtalk/flutter_dingtalk.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterDingtalk.registerApp('ding29oylailm5wkavgj' , 'com.yuhao.laobanmeng.server');
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  bool _isInstall = false;
  @override
  void initState() {
    super.initState();
    initPlatformState();
    isInstall();
    FlutterDingtalk.ddResponseEventHandler.listen((event) {
      print('event $event');
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterDingtalk.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future<void> isInstall() async {
    bool isInstall;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      isInstall = await FlutterDingtalk.isDingDingInstalled();
    } on PlatformException {
      isInstall = false;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _isInstall = isInstall;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Text('Running on: $_platformVersion\n'),
              Text('isInstall: $_isInstall\n'),
              TextButton(
                  onPressed: (){
                  FlutterDingtalk.sendAuth('auth');
              },
                  child: Text('anth'))
            ],
          ),
        ),
      ),
    );
  }
}
