#import <Flutter/Flutter.h>
@interface FlutterDingtalkPlugin : NSObject<FlutterPlugin>
- (instancetype) initWithFlutterPluginRegistrar: (NSObject <FlutterPluginRegistrar> *) registrar;

@end
