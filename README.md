# flutter_dingtalk

钉钉 Flutter 插件,支持IOS,Android 端授权 支持null-safe
按照钉钉官方文档配置项目
### 参考钉钉官方文档
[钉钉官方文档](https://ding-doc.dingtalk.com/doc#/native/oguxo2)
## 使用
```dart
//注册钉钉插件
FlutterDingtalk.registerApp('Your AppId', 'Your IOS BundleId')

//钉钉是否安装
FlutterDingtalk.isDDAppInstalled()

//钉钉授权
FlutterDingtalk.sendDDAppAuth('state');

//分享文本
FlutterDingtalk.sendTextMessage('分享文本');

//分享图片
FlutterDingtalk.sendImageMessage(picUrl:'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png');

//分享链接
FlutterDingtalk.sendWebPageMessage('https://www.baidu.com/',title: '标题',content: '描述2333',
                                  thumbUrl:'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png');

// 回调
FlutterDingtalk.ddResponseEventHandler.listen((resp) async {
  //授权回调信息
  if (resp is DDShareAuthResponse) {
    print('授权回调信息=====> code: ${resp.code}  state:${resp.state}');
  }
});

```